# WordPress
Please visit [WordPress](https://wordpress.org/) for more info.

_WordPress_ is the new way to design a robust, speedy-fast and user-friendly website. 
Whether It's a new online shop or a professional portfolio, It got it all.  
It also makes it easy for everyone to design their preferable website using 
powerful drag and drop tools and plugins. 
### Why WordPress?
* Out-of-the-box management panel
* Thousands of plugins updated on a monthly basis (If not weekly)
* SEO-friendly (As a matter of fact some call it the most SEO-friendly CMS)
* Dozens of importable demo templates ready to go at the [Snapp!](https://snapp.ir/) 
of a finger

## Getting Started
To get started with _WordPress_, the platform first needs to be hosted on a server. 
There is a vast variety of services with different pricing each person can adopt.  
This doc is focused on serving _WordPress_ on a personal linux server owned by the user.

There are `three` key components to each _WordPress_ website.
* Database
* WP Config
* Web Server

### Database
As _WordPress_ is developed with pure **[PHP](https://www.php.net/)**, 
It best works with **[MySQL](https://www.mysql.com/)** or **[MariaDb](https://mariadb.org/)**
databases. Note that installing _WordPress_ on these databases does not require advanced 
database knowledge.  
Following is a simple tutorial on how to create a database in MySQL 
and assign a privileged user to it.

After installing `MySQL` on your linux machine, enter the below command to enter 
MySQL terminal environment:
```console
$ sudo mysql
or (for authenticated entry)
$ mysql -u root -p
```
Once logged in, use the below command to create a database:
```console
mysql> CREATE DATABASE wordpress;
```
Now create the user:
```console
mysql> CREATE USER 'username'@'localhost' IDENTIFIED BY 'password';
```
And then assign all the privileges of the created database to the user just created:
```console
mysql> GRANT ALL ON wordpress.* TO 'username'@'localhost';
```
Done! Now the database required for the _WordPress_ website is up and running.

### WordPress Configuration
There is a single configuration file required to be filled by the user for 
every _WordPress_ website.
The file is initially named `wp_config_sample.php` and can be found in the 
root of each _WordPress_ directory.  

First rename `wp_config_sample.php` to `wp_config.php`.
```console
$ sudo mv wp_config_sample.php wp_config.php
```
Now let's open and configure this file:

Replace `database_name_here` with the database name created earlier.
```php
define( 'DB_NAME', 'database_name_here' );
```
Replace `username_here` with the username assigned to the database above.
```php
define( 'DB_USER', 'username_here' );
```
Replace `password_here` with the password assigned to the username above.
```php
define( 'DB_PASSWORD', 'password_here' );
```
This section determines which host the database is accessible on. for basic users 
this host is usually `localhost`, but note that this can vary for you 
in the future.
```php
define( 'DB_HOST', 'localhost' );
```
The final step is to add a set of unique keys and salts to your configuration 
in order to increase the security of your _WordPress_ website.  

Visit [this API](https://api.wordpress.org/secret-key/1.1/salt/) to get your 
unique keys.  
Now copy all the displayed keys and replace the below snippet in `wp_config.php` 
with the copied keys.
```console
define( 'AUTH_KEY',         'put your unique phrase here' );
define( 'SECURE_AUTH_KEY',  'put your unique phrase here' );
define( 'LOGGED_IN_KEY',    'put your unique phrase here' );
define( 'NONCE_KEY',        'put your unique phrase here' );
define( 'AUTH_SALT',        'put your unique phrase here' );
define( 'SECURE_AUTH_SALT', 'put your unique phrase here' );
define( 'LOGGED_IN_SALT',   'put your unique phrase here' );
define( 'NONCE_SALT',       'put your unique phrase here' );
```
Done! The _WordPress_ `wp_config.php` file is successfully configured.

### Webserver Configuration
For a sample configuration of `Nginx` please visit `Step 3 - Configuring Nginx` of this 
[Link](https://www.digitalocean.com/community/tutorials/how-to-install-wordpress-with-lemp-on-ubuntu-20-04)

## Installing WordPress
Now navigate to the server's domain name or the IP address assigned to 
the _WordPress_ website.

Select the preferred language:

![Choose Language Page](./docAssets/lang.png)

Fill out the required info:  
Note that these credentials can be changed later on.

![Initial Info Page](./docAssets/info.png)

Enter this `/wp-admin` after the domain to 
log in.
(ex. `www.example.com/wp-admin` or `localhost/wp-admin`)  
Now enter the `Username` and `Password` to access admin panel.

![Login Page](./docAssets/login.png)

## Conclusions
Congratulations! _WordPress_ website is _ready to go_!  
For more info on how to use this panel and customize 
_WordPress_, Keep being updated with this 
[repo](https://gitlab.com/thatsSoheil/wordpress).

![Admin Panel Page](./docAssets/panel.png)

## License
Copyright (c) 2021 Soheil Fakour

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
